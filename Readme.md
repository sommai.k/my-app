# expo-template-native-base-typescript

The official NativeBase TypeScript template for [Expo](https://docs.expo.io/)

## Usage

```sh
expo init my-app --template expo-template-native-base-typescript
```

## navigation

```
yarn add @react-navigation/native
expo install react-native-screens
expo install react-native-safe-area-context
yarn add @react-navigation/native-stack
yarn add @react-navigation/bottom-tabs
```

## icons

```
expo install @expo/vector-icons
```

[browse icons](https://icons.expo.fyi/)

## redux state management

```
yarn add @reduxjs/toolkit
yarn add react-redux
yarn add @types/react-redux
```

## axios

```
yarn add axios
```
