import React from "react";
import {
  Text,
  Link,
  HStack,
  Center,
  Heading,
  Switch,
  useColorMode,
  NativeBaseProvider,
  extendTheme,
  VStack,
  Code,
} from "native-base";
import NativeBaseIcon from "./components/NativeBaseIcon";
import HelloWorld from "./components/hello/HelloWorld";
import HelloView from "./components/hello/HelloView";
import TitleText from "./components/hello/TitleText";
import HelloTitle from "./components/hello/HelloTitle";
import { store } from "./app/store";
import { Provider } from "react-redux";
import CounterDisplay from "./components/hello/CounterDisplay";
import CounterButton from "./components/hello/CounterButton";
import MainScreen from "./components/MainScreen";
// Define the config
const config = {
  useSystemColorMode: false,
  initialColorMode: "dark",
};

// extend the theme
export const theme = extendTheme({ config });

export default function App() {
  return (
    <Provider store={store}>
      <NativeBaseProvider>
        <MainScreen></MainScreen>
      </NativeBaseProvider>
    </Provider>
  );
}

// Color Switch Component
function ToggleDarkMode() {
  const { colorMode, toggleColorMode } = useColorMode();
  return (
    <HStack space={2} alignItems="center">
      <Text>Dark</Text>
      <Switch
        isChecked={colorMode === "light" ? true : false}
        onToggle={toggleColorMode}
        aria-label={
          colorMode === "light" ? "switch to dark mode" : "switch to light mode"
        }
      />
      <Text>Light</Text>
    </HStack>
  );
}
