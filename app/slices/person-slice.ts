import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import * as personApi from "../api/person";
// declare model

interface StateModel {
  isLoading: boolean;
  isError: boolean;
  error: string;
  persons: personApi.PersonModel[];
}

const initialState: StateModel = {
  isLoading: false,
  isError: false,
  error: "",
  persons: [],
};

// declare async action
const getAllPerson = createAsyncThunk("person/get-all-person", async () => {
  const resp = await personApi.getAll();
  return resp.data;
});

const getPersonById = createAsyncThunk(
  "person/get-by-id",
  async (id: number, thunk) => {
    const resp = await personApi.getById(id);
    return resp.data;
  }
);

const createPerson = createAsyncThunk(
  "person/create-person",
  async (person: personApi.NewPersonModel, thunk) => {
    const resp = await personApi.createPerson(person);
    thunk.dispatch(getAllPerson());
    return resp.data;
  }
);

const updatePersonById = createAsyncThunk(
  "person/update-by-id",
  async (person: personApi.UpdPersonModel, thunk) => {
    const resp = await personApi.updatePersonById(person);
    thunk.dispatch(getAllPerson());
    return resp.data;
  }
);

const delPersonById = createAsyncThunk(
  "person/del-by-id",
  async (id: number, thunk) => {
    const resp = await personApi.deleteById(id);
    thunk.dispatch(getAllPerson());
    return resp.data;
  }
);

// declare slice
const personSlice = createSlice({
  name: "person",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getAllPerson.fulfilled, (state, action) => {
        // state.persons = [];
        state.persons = [...action.payload];
      })
      .addMatcher(
        (action) => {
          const type = action.type as string;
          return type.endsWith("/fulfilled");
        },
        (state, action) => {
          state.isLoading = false;
          state.isError = false;
          state.error = "";
        }
      )
      .addMatcher(
        (action) => {
          const type = action.type as string;
          return type.endsWith("/pending");
        },
        (state, action) => {
          state.isLoading = true;
          state.isError = false;
          state.error = "";
        }
      )
      .addMatcher(
        (action) => {
          const type = action.type as string;
          return type.endsWith("/rejected");
        },
        (state, action) => {
          state.isLoading = false;
          state.isError = true;
          state.error = action.error.message;
        }
      );
  },
});

// export
export const personReducer = personSlice.reducer;
export {
  getAllPerson,
  getPersonById,
  createPerson,
  updatePersonById,
  delPersonById,
};
