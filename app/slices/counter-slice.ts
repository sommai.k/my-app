import { createSlice } from "@reduxjs/toolkit";

const counterSlice = createSlice({
  name: "counter",
  initialState: {
    value: 1,
  },
  reducers: {
    increment: (state) => {
      state.value = state.value + 1;
    },
  },
});

export const { increment } = counterSlice.actions;
export const counterReducer = counterSlice.reducer;
