import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import countryApi from "../country-api";

const allCountry = createAsyncThunk(
  "register/allCountry",
  async (args, trunk) => {
    const resp = await countryApi.get("/all");
    return resp.data;
  }
);

const registerSlice = createSlice({
  name: "register",
  initialState: {
    isProcessing: false,
    isError: false,
    error: "",
    countries: [],
  },
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(allCountry.pending, (state, action) => {
        state.isProcessing = true;
      })
      .addCase(allCountry.fulfilled, (state, action) => {
        state.isProcessing = false;
        state.countries = action.payload;
      })
      .addCase(allCountry.rejected, (state, action) => {
        state.isProcessing = false;
        state.isError = true;
        state.error = action.error.message ?? "";
      });
  },
});

export const registerReducer = registerSlice.reducer;
export { allCountry };
