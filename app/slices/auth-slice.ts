import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import httpApi from "../http-api";
interface UserLogin {
  username: string;
  password: string;
}

const login = createAsyncThunk("auth/login", async (userLogin: UserLogin) => {
  const resp = await httpApi.get("/users/" + userLogin.username);
  return resp.data;
});

const authSlice = createSlice({
  name: "auth",
  initialState: {
    isProcessing: false,
    isError: false,
    error: "",
  },
  reducers: {
    resetError: (state) => {
      state.isError = false;
      state.error = "";
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(login.pending, (state, action) => {
        state.isProcessing = true;
      })
      .addCase(login.fulfilled, (state, action) => {
        state.isProcessing = false;
      })
      .addCase(login.rejected, (state, action) => {
        state.isProcessing = false;
        state.isError = true;
        state.error = action.error.message ?? "";
      });
  },
});

export const { resetError } = authSlice.actions;
export { login };
export const authReducer = authSlice.reducer;
