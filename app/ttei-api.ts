import axios from "axios";

const tteiApi = axios.create();
// tteiApi.defaults.baseURL = "http://is-poom-nb.toshiba-ttei.com/Reactapi";
// tteiApi.defaults.baseURL = "http://172.16.111.4/Reactapi/api";
tteiApi.defaults.baseURL = "http://localhost:3000/Reactapi";
tteiApi.defaults.responseEncoding = "application/json";

export default tteiApi;
