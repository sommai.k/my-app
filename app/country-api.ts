import axios from "axios";

const countryApi = axios.create();
countryApi.defaults.baseURL = "https://restcountries.com/v2";
countryApi.defaults.responseEncoding = "application/json";

export default countryApi;
