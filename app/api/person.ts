import tteiApi from "../ttei-api";
// Declare data model
interface NewPersonModel {
  FirstName: string;
  LastName: string;
  file: string;
  ProvinceName: string;
  DistrictName: string;
}

interface UpdPersonModel extends NewPersonModel {
  id: number;
}

interface PersonModel {
  id: number;
  FirstName: string;
  LastName: string;
  PictureName: string;
  ProvinceName: string;
  DistrictName: string;
  PictureData: string;
  PictureDatas: string;
  CreateBy: string;
  CreateDate: string;
}

// Declare method

const getAll = () => {
  return tteiApi.get<PersonModel[]>("/api/TestReact/all");
};

const getById = (id: number) => {
  const url = `/api/TestReact?id=${id}`;
  return tteiApi.get<PersonModel>(url);
};

const createPerson = async (person: NewPersonModel) => {
  const url = "/api/TestReact/ins";
  const formData = new FormData();
  formData.append("FirstName", person.FirstName);
  formData.append("LastName", person.LastName);
  formData.append("ProvinceName", person.ProvinceName);
  formData.append("DistrictName", person.DistrictName);
  const resp = await fetch(person.file);
  const fblob = await resp.blob();
  const file = new File([fblob], "file_name.jpg");
  formData.append("file", file);
  return tteiApi.post(url, formData, {
    headers: { "Content-Type": "multipart/form-data" },
  });
};

const updatePersonById = async (person: UpdPersonModel) => {
  const url = "/api/TestReact/upd";
  const formData = new FormData();
  formData.append("id", `${person.id}`);
  formData.append("FirstName", person.FirstName);
  formData.append("LastName", person.LastName);
  formData.append("ProvinceName", person.ProvinceName);
  formData.append("DistrictName", person.DistrictName);
  const resp = await fetch(person.file);
  const fblob = await resp.blob();
  const file = new File([fblob], "file_name.jpg");
  formData.append("file", file);
  return tteiApi.put(url, formData, {
    headers: { "Content-Type": "multipart/form-data" },
  });
};

const deleteById = (id: number) => {
  const url = `/api/TestReact/del?id=${id}`;
  return tteiApi.delete(url);
};

// export
export {
  PersonModel,
  UpdPersonModel,
  NewPersonModel,
  getAll,
  getById,
  createPerson,
  updatePersonById,
  deleteById,
};
