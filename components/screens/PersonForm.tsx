import React, { useState, useEffect } from "react";
import {
  ScrollView,
  Center,
  Box,
  VStack,
  FormControl,
  Input,
  Button,
  Image,
} from "native-base";
import {
  NewPersonModel,
  PersonModel,
  UpdPersonModel,
} from "../../app/api/person";
import * as ImagePicker from "expo-image-picker";
import { useDispatch } from "react-redux";
import {
  createPerson,
  getPersonById,
  updatePersonById,
} from "../../app/slices/person-slice";
import { NavigationProp, Route } from "@react-navigation/native";
import { useAppDispatch } from "../../app/hook";
import { AppDispatch } from "../../app/store";
// Declare Model
const initialModel: NewPersonModel = {
  FirstName: "",
  LastName: "",
  file: "",
  ProvinceName: "",
  DistrictName: "",
};

// Declare Model
interface Param {
  isEdit: boolean;
  id: number;
}

interface Props {
  navigation: NavigationProp<any, any>;
  route: Route<string, Param>;
}
// Declare Component
const PersonForm: React.FC<Props> = ({ navigation, route }) => {
  const [formData, setFormData] = useState<NewPersonModel>(initialModel);
  const [errors, setErrors] = useState<NewPersonModel>(initialModel);
  const [mounted, setMounted] = useState<boolean>(false);
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (!mounted) {
      setMounted(true);
      if (route.params && route.params.isEdit) {
        dispatch(getPersonById(route.params.id)).then((action) => {
          if (action.payload) {
            const person = action.payload as PersonModel;
            const image = "data:image/jpeg;base64," + person.PictureData;
            setFormData({ ...person, file: image });
          }
        });
      }
    }
  });

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      setFormData({ ...formData, file: result.uri });
    }
  };

  const onSave = () => {
    if (route.params && route.params.isEdit) {
      const updForm: UpdPersonModel = { ...formData, id: route.params.id };
      dispatch(updatePersonById(updForm));
    } else {
      dispatch(createPerson(formData));
    }
    navigation.goBack();
  };

  return (
    <>
      <ScrollView>
        <Center>
          <Box safeArea p="2" py="8" w="90%" maxW="390">
            <VStack space="3">
              <FormControl
                isRequired={true}
                isInvalid={errors?.FirstName ? true : false}
              >
                <FormControl.Label>First Name</FormControl.Label>
                <Input
                  defaultValue={formData.FirstName}
                  onChangeText={(FirstName) =>
                    setFormData({ ...formData, FirstName })
                  }
                />
                <FormControl.ErrorMessage>
                  {errors?.FirstName}
                </FormControl.ErrorMessage>
              </FormControl>
              <FormControl
                isRequired={true}
                isInvalid={errors?.LastName ? true : false}
              >
                <FormControl.Label>Last Name</FormControl.Label>
                <Input
                  defaultValue={formData.LastName}
                  onChangeText={(LastName) =>
                    setFormData({ ...formData, LastName })
                  }
                />
                <FormControl.ErrorMessage>
                  {errors?.LastName}
                </FormControl.ErrorMessage>
              </FormControl>
              <FormControl
                isRequired={true}
                isInvalid={errors?.ProvinceName ? true : false}
              >
                <FormControl.Label>Province Name</FormControl.Label>
                <Input
                  defaultValue={formData.ProvinceName}
                  onChangeText={(ProvinceName) =>
                    setFormData({ ...formData, ProvinceName })
                  }
                />
                <FormControl.ErrorMessage>
                  {errors?.ProvinceName}
                </FormControl.ErrorMessage>
              </FormControl>
              <FormControl
                isRequired={true}
                isInvalid={errors?.DistrictName ? true : false}
              >
                <FormControl.Label>District Name</FormControl.Label>
                <Input
                  defaultValue={formData.DistrictName}
                  onChangeText={(DistrictName) =>
                    setFormData({ ...formData, DistrictName })
                  }
                />
                <FormControl.ErrorMessage>
                  {errors?.DistrictName}
                </FormControl.ErrorMessage>
              </FormControl>
              {!formData.file ? (
                <Button onPress={() => pickImage()}>Pick Image</Button>
              ) : (
                <VStack space="2">
                  <Image
                    alt="upload image"
                    size="128px"
                    source={{
                      uri: formData.file,
                    }}
                  />
                  <Button
                    colorScheme="red"
                    onPress={() => setFormData({ ...formData, file: "" })}
                  >
                    Remove Image
                  </Button>
                </VStack>
              )}
              <Button onPress={() => onSave()}>Save</Button>
            </VStack>
          </Box>
        </Center>
      </ScrollView>
    </>
  );
};

// Export
export default PersonForm;
