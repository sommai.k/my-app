import { Ionicons } from "@expo/vector-icons";
import { NavigationProp } from "@react-navigation/native";
import {
  Center,
  Box,
  Heading,
  VStack,
  FormControl,
  Input,
  Link,
  HStack,
  Text,
  Button,
  Icon,
  Modal,
  Spinner,
} from "native-base";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { login, resetError } from "../../app/slices/auth-slice";
import { RootState } from "../../app/store";

interface Props {
  navigation: NavigationProp<any, any>;
}

interface LoginForm {
  email?: string;
  password?: string;
}

const Login: React.FC<Props> = ({ navigation }) => {
  const [userForm, setUserForm] = useState<LoginForm>();
  const [showPassword, setShowPassword] = useState<boolean>(false);
  const [errors, setErrors] = useState<LoginForm>();
  const dispatch = useDispatch();
  const auth = useSelector((state: RootState) => state.auth);

  const signIn = () => {
    if (validate()) {
      console.log(userForm?.email, userForm?.password);

      dispatch(
        login({
          username: userForm?.email ?? "",
          password: userForm?.password ?? "",
        })
      );
    } else {
      console.log("Invalid form");
    }
  };

  const validate = () => {
    let valid = true;
    let errorForm: LoginForm = {};
    if (userForm?.email === undefined || userForm?.email.length === 0) {
      errorForm.email = "Email is required";
      valid = false;
    }

    if (userForm?.password === undefined || userForm?.password.length === 0) {
      errorForm.password = "Password is required";
      valid = false;
    }

    if (valid) {
      setErrors({});
    } else {
      setErrors(errorForm);
    }

    return valid;
  };

  return (
    <Center w="100%">
      <Box safeArea p="2" py="8" w="90%" maxW="290">
        <Heading
          size="lg"
          fontWeight="600"
          color="coolGray.800"
          _dark={{
            color: "warmGray.50",
          }}
        >
          Welcome
        </Heading>
        <Heading
          mt="1"
          _dark={{
            color: "warmGray.200",
          }}
          color="coolGray.600"
          fontWeight="medium"
          size="xs"
        >
          Sign in to continue!
        </Heading>

        <VStack space={3} mt="5">
          <FormControl
            isRequired={true}
            isInvalid={errors?.email ? true : false}
          >
            <FormControl.Label>Email ID</FormControl.Label>
            <Input
              onChangeText={(email) => setUserForm({ ...userForm, email })}
            />
            <FormControl.ErrorMessage>{errors?.email}</FormControl.ErrorMessage>
          </FormControl>
          <FormControl
            isRequired={true}
            isInvalid={errors?.password ? true : false}
          >
            <FormControl.Label>Password</FormControl.Label>
            <Input
              type={showPassword ? "text" : "password"}
              onChangeText={(password) =>
                setUserForm({ ...userForm, password })
              }
              InputRightElement={
                <Icon
                  as={Ionicons}
                  name={showPassword ? "eye" : "eye-off"}
                  size="sm"
                  onPress={() => setShowPassword(!showPassword)}
                />
              }
            />
            <FormControl.ErrorMessage>
              {errors?.password}
            </FormControl.ErrorMessage>
            <Link
              _text={{
                fontSize: "xs",
                fontWeight: "500",
                color: "indigo.500",
              }}
              alignSelf="flex-end"
              mt="1"
            >
              Forget Password?
            </Link>
          </FormControl>
          <Button mt="2" colorScheme="indigo" onPress={() => signIn()}>
            Sign in
          </Button>
          <HStack mt="6" justifyContent="center">
            <Text
              fontSize="sm"
              color="coolGray.600"
              _dark={{
                color: "warmGray.200",
              }}
            >
              I'm a new user.{" "}
            </Text>
            <Link
              _text={{
                color: "indigo.500",
                fontWeight: "medium",
                fontSize: "sm",
              }}
              onPress={() => navigation.navigate("Register")}
              href="#"
            >
              Sign Up
            </Link>
          </HStack>
        </VStack>
      </Box>
      <Modal isOpen={auth.isProcessing}>
        <Modal.Content>
          <Modal.Header>Processing</Modal.Header>
          <Modal.Body>
            <Spinner size="lg" />
          </Modal.Body>
        </Modal.Content>
      </Modal>
      <Modal isOpen={auth.isError}>
        <Modal.Content>
          <Modal.Header>Error</Modal.Header>
          <Modal.Body>{auth.error}</Modal.Body>
          <Modal.Footer>
            <Button.Group>
              <Button onPress={() => dispatch(resetError())}>OK</Button>
            </Button.Group>
          </Modal.Footer>
        </Modal.Content>
      </Modal>
    </Center>
  );
};

export default Login;
