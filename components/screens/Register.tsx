import React, { useEffect, useState } from "react";
import {
  Box,
  Button,
  Center,
  Checkbox,
  FormControl,
  HStack,
  Input,
  Radio,
  ScrollView,
  Select,
  Spacer,
  Switch,
  Text,
  TextArea,
  VStack,
} from "native-base";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../app/store";
import { allCountry } from "../../app/slices/register-slice";

interface RegisterForm {
  email?: string;
  name?: string;
  sex?: string;
  country?: string;
  phone?: string;
  address?: string;
  agree: boolean;
  isRobot: boolean;
  age?: number;
}

interface ErrorForm {
  age?: string;
  email?: string;
  agree?: string;
  isRobot?: string;
}

const Register: React.FC = () => {
  const [formData, setFormData] = useState<RegisterForm>({
    agree: false,
    isRobot: false,
    country: "TH",
  });

  const [errors, setErrors] = useState<ErrorForm>();
  const [mounted, setMounted] = useState(false);

  const dispatch = useDispatch();
  const register = useSelector((state: RootState) => state.register);

  useEffect(() => {
    dispatch(allCountry());
    setMounted(true);
  }, [mounted]);

  const validate = () => {
    let valid = true;
    let errorForm: ErrorForm = {};

    if (formData?.email === undefined || formData?.email.length === 0) {
      errorForm.email = "Email is required";
      valid = false;
    }

    if (!formData.agree) {
      errorForm.agree = "You must agree before register";
      valid = false;
    }

    if (!formData.isRobot) {
      errorForm.isRobot = "You are robot";
      valid = false;
    }

    if (
      formData?.age === undefined ||
      formData?.age < 18 ||
      isNaN(formData?.age)
    ) {
      errorForm.age = "Age must be over than 18";
      valid = false;
    }

    if (valid) {
      setErrors({});
    } else {
      setErrors(errorForm);
    }

    return valid;
  };

  const onRegister = () => {
    if (validate()) {
      console.log(JSON.stringify(formData));
    } else {
      console.log("Invalid form");
    }
  };

  return (
    <ScrollView>
      <Center>
        <Box safeArea p="2" py="8" w="90%" maxW="390">
          <VStack space="3">
            <FormControl
              isRequired={true}
              isInvalid={errors?.email ? true : false}
            >
              <FormControl.Label>Email</FormControl.Label>
              <Input
                onChangeText={(email) => setFormData({ ...formData, email })}
              />
              <FormControl.ErrorMessage>
                {errors?.email}
              </FormControl.ErrorMessage>
            </FormControl>

            <FormControl>
              <FormControl.Label>Name</FormControl.Label>
              <Input
                onChangeText={(name) => setFormData({ ...formData, name })}
              />
            </FormControl>
            <FormControl>
              <FormControl.Label>Sex</FormControl.Label>
              <Radio.Group
                name="sex"
                onChange={(sex) => setFormData({ ...formData, sex })}
                value={formData.sex}
              >
                <HStack space={3}>
                  <Radio value="M">Male</Radio>
                  <Radio value="F">Female</Radio>
                </HStack>
              </Radio.Group>
            </FormControl>

            <FormControl
              isRequired={true}
              isInvalid={errors?.age ? true : false}
            >
              <FormControl.Label>Age</FormControl.Label>
              <Input
                keyboardType="numeric"
                onChangeText={(age) =>
                  setFormData({ ...formData, age: parseInt(age) })
                }
              />
              <FormControl.ErrorMessage>{errors?.age}</FormControl.ErrorMessage>
            </FormControl>
            <FormControl>
              <FormControl.Label>Country</FormControl.Label>
              <Select
                selectedValue={formData.country}
                onValueChange={(country) =>
                  setFormData({ ...formData, country })
                }
              >
                <Select.Item
                  value=""
                  label="Please choose country"
                ></Select.Item>
                {register.countries.map((value: any, index: number) => (
                  <Select.Item
                    key={index}
                    value={value.alpha2Code}
                    label={value.name}
                  ></Select.Item>
                ))}
              </Select>
            </FormControl>
            <FormControl>
              <FormControl.Label>Phone</FormControl.Label>
              <Input
                onChangeText={(phone) => setFormData({ ...formData, phone })}
              />
            </FormControl>
            <FormControl>
              <FormControl.Label>Address</FormControl.Label>
              <TextArea
                onChangeText={(address) =>
                  setFormData({ ...formData, address })
                }
              ></TextArea>
            </FormControl>
            <FormControl isInvalid={errors?.agree ? true : false}>
              <Checkbox
                value="true"
                onChange={(agree) => setFormData({ ...formData, agree })}
              >
                Agree with term
              </Checkbox>
              <FormControl.ErrorMessage>
                {errors?.agree}
              </FormControl.ErrorMessage>
            </FormControl>

            <FormControl isInvalid={errors?.isRobot ? true : false}>
              <HStack>
                <Text>Is Robot</Text>
                <Spacer />
                <Switch
                  isChecked={formData.isRobot}
                  onToggle={() =>
                    setFormData({ ...formData, isRobot: !formData.isRobot })
                  }
                ></Switch>
              </HStack>
              <FormControl.ErrorMessage>
                {errors?.isRobot}
              </FormControl.ErrorMessage>
            </FormControl>

            <Button onPress={() => onRegister()}>Register</Button>
          </VStack>
        </Box>
      </Center>
    </ScrollView>
  );
};

export default Register;
