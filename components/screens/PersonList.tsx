import { Ionicons } from "@expo/vector-icons";
import { NavigationProp } from "@react-navigation/native";
import {
  Avatar,
  Divider,
  FlatList,
  HStack,
  Text,
  Modal,
  Spinner,
  IconButton,
  Icon,
  Button,
} from "native-base";
import React, { useEffect, useLayoutEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { PersonModel } from "../../app/api/person";
import { delPersonById, getAllPerson } from "../../app/slices/person-slice";
import { RootState } from "../../app/store";

// Declare Model
interface Props {
  navigation: NavigationProp<any, any>;
}

// Declare Component
const PersonList: React.FC<Props> = ({ navigation }) => {
  const dispatch = useDispatch();
  const [mounted, setMounted] = useState<boolean>(false);
  useEffect(() => {
    if (!mounted) {
      dispatch(getAllPerson());
      setMounted(true);
    }
  });

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <HStack>
          <IconButton
            icon={<Icon as={Ionicons} name="ios-add-circle" />}
            onPress={() => navigation.navigate("PersonForm")}
          ></IconButton>
        </HStack>
      ),
    });
  });

  const person = useSelector((state: RootState) => state.person);
  const renderItem = ({ item }: { item: PersonModel }) => (
    <>
      <HStack space="3" px="5" py="2">
        {item.PictureData ? (
          <Avatar
            source={{ uri: `data:image/png;base64,${item.PictureData}` }}
          />
        ) : (
          <></>
        )}
        <Text>{item.FirstName}</Text>
        <Button
          onPress={() =>
            navigation.navigate("PersonForm", {
              id: item.id,
              isEdit: true,
            })
          }
        >
          Edit
        </Button>
        <Button onPress={() => dispatch(delPersonById(item.id))}>Del</Button>
      </HStack>
      <Divider />
    </>
  );
  return (
    <>
      <FlatList
        initialNumToRender={5}
        maxToRenderPerBatch={10}
        windowSize={10}
        scrollEnabled={true}
        h="96"
        data={person.persons}
        keyExtractor={(item: PersonModel) => `${item.id}`}
        renderItem={renderItem}
      ></FlatList>
      <Modal isOpen={person.isLoading}>
        <Modal.Content>
          <Modal.Header>Loading</Modal.Header>
          <Modal.Body>
            <Spinner size="lg" />
          </Modal.Body>
        </Modal.Content>
      </Modal>
    </>
  );
};

// Export
export default PersonList;
