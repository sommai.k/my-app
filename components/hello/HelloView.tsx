import React from "react";
import { View } from "native-base";

interface Props {
  children: React.ReactNode;
}

const HelloView: React.FC<Props> = ({ children }) => {
  return (
    <>
      <View>{children}</View>
    </>
  );
};

export default HelloView;
