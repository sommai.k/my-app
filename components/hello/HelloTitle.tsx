import React, { useState } from "react";
import { Text, Button } from "native-base";

interface User {
  code: string;
  name: string;
}

const HelloTitle: React.FC = () => {
  const [cnt, setCnt] = useState<number>(1);
  const [age, setAge] = useState<number>(70);
  const [user, setUser] = useState<User[]>([
    {
      code: "",
      name: "",
    },
  ]);

  const increase = () => {
    setCnt(10);
  };

  return (
    <>
      <Text>
        FC Value {cnt} and {age} name {user[0].code}
      </Text>
      <Button onPress={() => setCnt(cnt + 1)}>FC Increase</Button>
    </>
  );
};

export default HelloTitle;
