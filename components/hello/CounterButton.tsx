import React from "react";
import { Button } from "native-base";
import { useDispatch } from "react-redux";
import { increment } from "../../app/slices/counter-slice";

const CounterButton: React.FC = () => {
  const dispatch = useDispatch();
  return (
    <>
      <Button onPress={() => dispatch(increment())}>Redux Increase</Button>
    </>
  );
};

export default CounterButton;
