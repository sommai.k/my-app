import { Text, Button } from "native-base";
import React from "react";

interface Props {
  title: string;
}

interface State {
  cnt: number;
}

class TitleText extends React.Component<Props, State> {
  // counter: number = 1;
  constructor(props: Props) {
    super(props);
    this.state = { cnt: 1 };
  }

  increase(): void {
    console.log("increase press");
    // this.counter += 1;
    // console.log(this.counter);
    this.setState({
      cnt: this.state.cnt + 1,
    });
  }

  render(): React.ReactNode {
    return (
      <>
        <Text>
          {this.props.title} {this.state.cnt}
        </Text>
        <Button onPress={() => this.increase()}>Increase</Button>
        <Button onPress={this.increase.bind(this)}>Add</Button>
      </>
    );
  }
}

export default TitleText;
