import React from "react";
import { Text, Center, Button } from "native-base";

interface Props {
  name?: string;
  title?: string;
}

const sayName = (n?: string) => {
  if (n) {
    return <Text>This is your name {n}</Text>;
  } else {
    return <Button>Noname</Button>;
  }
};

const element = (name?: string, title?: string) => (
  <>
    <Center>
      {sayName(name)}
      <Text>{title}</Text>
    </Center>
  </>
);

const HelloWorld: React.FC<Props> = ({ name, title }) => {
  return element(name, title);
};

export default HelloWorld;
