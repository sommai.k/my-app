import React from "react";
import { Text } from "native-base";
import { useSelector } from "react-redux";
import { RootState } from "../../app/store";

const CounterDisplay: React.FC = () => {
  const counter = useSelector((state: RootState) => state.counter);

  return (
    <>
      <Text>Counter is {counter.value}</Text>
    </>
  );
};

export default CounterDisplay;
