import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Login from "./screens/Login";
import Register from "./screens/Register";
import PersonList from "./screens/PersonList";
import PersonForm from "./screens/PersonForm";

const Stack = createNativeStackNavigator();

const MainScreen: React.FC = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="PersonList" component={PersonList} />
        <Stack.Screen name="PersonForm" component={PersonForm} />
        <Stack.Screen
          name="Login"
          component={Login}
          options={{ headerShown: false }}
        ></Stack.Screen>
        <Stack.Screen name="Register" component={Register}></Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default MainScreen;
